import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class SuDoKu extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel pnlContainer, pnlOfLbls, pnlSolveIt, pnlInputNumber;
	private JLabel lblInputNumber;
	private JTextField txtInputNumber;
	private JButton btnSolveIt;
	private JLabel[][] lblArray;

	// private List<Integer> lstNumbers;
	// private List<List<String>> group;

	private List<Integer> lstPos1, lstPos2, lstPos3, lstPos4, lstPos5, lstPos6,
			lstPos7, lstPos8, lstPos9, lstPos10, lstPos11, lstPos12, lstPos13,
			lstPos14, lstPos15, lstPos16, lstPos17, lstPos18, lstPos19,
			lstPos20, lstPos21, lstPos22, lstPos23, lstPos24, lstPos25,
			lstPos26, lstPos27, lstPos28, lstPos29, lstPos30, lstPos31,
			lstPos32, lstPos33, lstPos34, lstPos35, lstPos36, lstPos37,
			lstPos38, lstPos39, lstPos40, lstPos41, lstPos42, lstPos43,
			lstPos44, lstPos45, lstPos46, lstPos47, lstPos48, lstPos49,
			lstPos50, lstPos51, lstPos52, lstPos53, lstPos54, lstPos55,
			lstPos56, lstPos57, lstPos58, lstPos59, lstPos60, lstPos61,
			lstPos62, lstPos63, lstPos64, lstPos65, lstPos66, lstPos67,
			lstPos68, lstPos69, lstPos70, lstPos71, lstPos72, lstPos73,
			lstPos74, lstPos75, lstPos76, lstPos77, lstPos78, lstPos79,
			lstPos80, lstPos81;

	public SuDoKu() {
		// lstNumbers=new ArrayList<>(9);
		// List<lstNumbers> group = new ArrayList<lstNumbers>(81);

		lstPos1 = new ArrayList<>(9);
		lstPos2 = new ArrayList<>(9);
		lstPos3 = new ArrayList<>(9);
		lstPos4 = new ArrayList<>(9);
		lstPos5 = new ArrayList<>(9);
		lstPos6 = new ArrayList<>(9);
		lstPos7 = new ArrayList<>(9);
		lstPos8 = new ArrayList<>(9);
		lstPos9 = new ArrayList<>(9);
		lstPos10 = new ArrayList<>(9);

		lstPos11 = new ArrayList<>(9);
		lstPos12 = new ArrayList<>(9);
		lstPos13 = new ArrayList<>(9);
		lstPos14 = new ArrayList<>(9);
		lstPos15 = new ArrayList<>(9);
		lstPos16 = new ArrayList<>(9);
		lstPos17 = new ArrayList<>(9);
		lstPos18 = new ArrayList<>(9);
		lstPos19 = new ArrayList<>(9);
		lstPos20 = new ArrayList<>(9);

		lstPos21 = new ArrayList<>(9);
		lstPos22 = new ArrayList<>(9);
		lstPos23 = new ArrayList<>(9);
		lstPos24 = new ArrayList<>(9);
		lstPos25 = new ArrayList<>(9);
		lstPos26 = new ArrayList<>(9);
		lstPos27 = new ArrayList<>(9);
		lstPos28 = new ArrayList<>(9);
		lstPos29 = new ArrayList<>(9);
		lstPos30 = new ArrayList<>(9);

		lstPos31 = new ArrayList<>(9);
		lstPos32 = new ArrayList<>(9);
		lstPos33 = new ArrayList<>(9);
		lstPos34 = new ArrayList<>(9);
		lstPos35 = new ArrayList<>(9);
		lstPos36 = new ArrayList<>(9);
		lstPos37 = new ArrayList<>(9);
		lstPos38 = new ArrayList<>(9);
		lstPos39 = new ArrayList<>(9);
		lstPos40 = new ArrayList<>(9);

		lstPos41 = new ArrayList<>(9);
		lstPos42 = new ArrayList<>(9);
		lstPos43 = new ArrayList<>(9);
		lstPos44 = new ArrayList<>(9);
		lstPos45 = new ArrayList<>(9);
		lstPos46 = new ArrayList<>(9);
		lstPos47 = new ArrayList<>(9);
		lstPos48 = new ArrayList<>(9);
		lstPos49 = new ArrayList<>(9);
		lstPos50 = new ArrayList<>(9);

		lstPos51 = new ArrayList<>(9);
		lstPos52 = new ArrayList<>(9);
		lstPos53 = new ArrayList<>(9);
		lstPos54 = new ArrayList<>(9);
		lstPos55 = new ArrayList<>(9);
		lstPos56 = new ArrayList<>(9);
		lstPos57 = new ArrayList<>(9);
		lstPos58 = new ArrayList<>(9);
		lstPos59 = new ArrayList<>(9);
		lstPos60 = new ArrayList<>(9);

		lstPos61 = new ArrayList<>(9);
		lstPos62 = new ArrayList<>(9);
		lstPos63 = new ArrayList<>(9);
		lstPos64 = new ArrayList<>(9);
		lstPos65 = new ArrayList<>(9);
		lstPos66 = new ArrayList<>(9);
		lstPos67 = new ArrayList<>(9);
		lstPos68 = new ArrayList<>(9);
		lstPos69 = new ArrayList<>(9);
		lstPos70 = new ArrayList<>(9);

		lstPos71 = new ArrayList<>(9);
		lstPos72 = new ArrayList<>(9);
		lstPos73 = new ArrayList<>(9);
		lstPos74 = new ArrayList<>(9);
		lstPos75 = new ArrayList<>(9);
		lstPos76 = new ArrayList<>(9);
		lstPos77 = new ArrayList<>(9);
		lstPos78 = new ArrayList<>(9);
		lstPos79 = new ArrayList<>(9);
		lstPos80 = new ArrayList<>(9);

		lstPos81 = new ArrayList<>(9);

		lblInputNumber = new JLabel("Input a number to this field: ");

		txtInputNumber = new JTextField();

		btnSolveIt = new JButton("Solve It!");
		btnSolveIt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				solveSuDoKu();
			}
		});

		pnlContainer = new JPanel();
		pnlContainer.setLayout(new BorderLayout());

		pnlOfLbls = new JPanel();
		pnlOfLbls.setLayout(new GridLayout(9, 9));

		pnlSolveIt = new JPanel();

		pnlInputNumber = new JPanel();
		pnlInputNumber.setLayout(new GridLayout(1, 2));

		pnlInputNumber.add(lblInputNumber);
		pnlInputNumber.add(txtInputNumber);

		lblArray = new JLabel[10][10];

		for (int row = 1; row < 10; row++) {
			for (int col = 1; col < 10; col++) {
				lblArray[row][col] = new JLabel("", SwingConstants.CENTER);
				// lblArray[row][col].setFont(new Font("Arial", 5, 5));

				final int intRow = row, intCol = col;
				lblArray[row][col].addMouseListener(new MouseListener() {

					@Override
					public void mouseReleased(MouseEvent arg0) {
					}

					@Override
					public void mousePressed(MouseEvent arg0) {
					}

					@Override
					public void mouseExited(MouseEvent arg0) {
					}

					@Override
					public void mouseEntered(MouseEvent arg0) {
					}

					@Override
					public void mouseClicked(MouseEvent arg0) {
						System.out.println("Clicked!"
								+ ((JLabel) arg0.getComponent()).getText());
						System.out.println("Row: " + intRow + ", Col: "
								+ intCol);

						int choice = JOptionPane.showConfirmDialog(null,
								pnlInputNumber, "Password Input Panel",
								JOptionPane.OK_CANCEL_OPTION);

						if (choice == JOptionPane.OK_OPTION) {
							((JLabel) arg0.getComponent())
									.setText(txtInputNumber.getText());
							((JLabel) arg0.getComponent()).setOpaque(true);
							((JLabel) arg0.getComponent())
									.setBackground(Color.ORANGE);

							// Remove all items from current list except the
							// entered one.
							mapRowsAndColsToList(intRow, intCol).clear();
							mapRowsAndColsToList(intRow, intCol).add(
									Integer.parseInt(txtInputNumber.getText()));

							// To print
							System.out.println("Rest of the ele:"
									+ mapRowsAndColsToList(intRow, intCol));
						}

						txtInputNumber.setText("");
					}
				});

				// lblArray[row][col].setText("Lbl("+row+","+col+")");
				lblArray[row][col].setBorder(BorderFactory
						.createLineBorder(new Color(0, 0, 0)));

				pnlOfLbls.add(lblArray[row][col]);

				// Initially add all numbers
				for (int i = 1; i < 10; i++) {
					insertElement(mapRowsAndColsToList(row, col), i);
				}
			}
		}

		// Just print -------
		/*
		 * for (int i = 0; i < 9; i++) {
		 * System.out.println("List elements: "+lstPos19.get(i).toString()); }
		 */
		// ------------------

		pnlSolveIt.add(btnSolveIt);

		pnlContainer.add(pnlOfLbls, BorderLayout.CENTER);
		pnlContainer.add(pnlSolveIt, BorderLayout.EAST);

		add(pnlContainer);

		setVisible(true);
		setSize(800, 700);
		setTitle("Array of Labels");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	int intFnCounter = 0;

	private void solveSuDoKu() {
		intFnCounter++;
		for (int row = 1; row < 10; row++) {
			for (int col = 1; col < 10; col++) {
				if (!lblArray[row][col].getText().equals("")) {
					locateRegeionLablesAndInputElement(row, col,
							Integer.parseInt(lblArray[row][col].getText()));
					lblArray[row][col].setOpaque(true);
					lblArray[row][col].setBackground(Color.ORANGE);
				}
			}
		}
		System.out.println("Fn Counter: " + intFnCounter);
	}

	private void locateRegeionLablesAndInputElement(int rowOfEle, int colOfEle,
			int Element) {
		for (int col = 1; col < 10; col++) {
			if (lblArray[rowOfEle][col].getText().equals("")) {
				System.out.println("Row Lables: Lbl(" + rowOfEle + "," + col
						+ ")");
				lblArray[rowOfEle][col].setOpaque(true);
				lblArray[rowOfEle][col].setBackground(Color.YELLOW);

				removeElementFromList(mapRowsAndColsToList(rowOfEle, col),
						Element);
			}
		}

		for (int row = 1; row < 10; row++) {
			if (lblArray[row][colOfEle].getText().equals("")) {
				System.out.println("Col Lables: Lbl(" + row + "," + colOfEle
						+ ")");
				lblArray[row][colOfEle].setOpaque(true);
				lblArray[row][colOfEle].setBackground(Color.YELLOW);

				removeElementFromList(mapRowsAndColsToList(row, colOfEle),
						Element);
			}
		}

		if (rowOfEle <= 3 && colOfEle <= 3) {
			for (int row = 1; row <= 3; row++) {
				for (int col = 1; col <= 3; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 3 && colOfEle <= 6) {
			for (int row = 1; row <= 3; row++) {
				for (int col = 4; col <= 6; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 3 && colOfEle <= 9) {
			for (int row = 1; row <= 3; row++) {
				for (int col = 7; col <= 9; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 6 && colOfEle <= 3) {
			for (int row = 4; row <= 6; row++) {
				for (int col = 1; col <= 3; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 6 && colOfEle <= 6) {
			for (int row = 4; row <= 6; row++) {
				for (int col = 4; col <= 6; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 6 && colOfEle <= 9) {
			for (int row = 4; row <= 6; row++) {
				for (int col = 7; col <= 9; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 9 && colOfEle <= 3) {
			for (int row = 7; row <= 9; row++) {
				for (int col = 1; col <= 3; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 9 && colOfEle <= 6) {
			for (int row = 7; row <= 9; row++) {
				for (int col = 4; col <= 6; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		} else if (rowOfEle <= 9 && colOfEle <= 9) {
			for (int row = 7; row <= 9; row++) {
				for (int col = 7; col <= 9; col++) {
					if (lblArray[row][col].getText().equals("")) {
						System.out.println("Quadrant Lables: Lbl(" + row + ","
								+ col + ")");
						lblArray[row][col].setOpaque(true);
						lblArray[row][col].setBackground(Color.YELLOW);

						removeElementFromList(mapRowsAndColsToList(row, col),
								Element);
					}
				}
			}
		}

	}

	private List<Integer> mapRowsAndColsToList(int row, int col) {
		if ((row == 1) && (col == 1))
			return lstPos1;
		else if ((row == 1) && (col == 2))
			return lstPos2;
		else if ((row == 1) && (col == 3))
			return lstPos3;
		else if ((row == 1) && (col == 4))
			return lstPos4;
		else if ((row == 1) && (col == 5))
			return lstPos5;
		else if ((row == 1) && (col == 6))
			return lstPos6;
		else if ((row == 1) && (col == 7))
			return lstPos7;
		else if ((row == 1) && (col == 8))
			return lstPos8;
		else if ((row == 1) && (col == 9))
			return lstPos9;

		else if ((row == 2) && (col == 1))
			return lstPos10;
		else if ((row == 2) && (col == 2))
			return lstPos11;
		else if ((row == 2) && (col == 3))
			return lstPos12;
		else if ((row == 2) && (col == 4))
			return lstPos13;
		else if ((row == 2) && (col == 5))
			return lstPos14;
		else if ((row == 2) && (col == 6))
			return lstPos15;
		else if ((row == 2) && (col == 7))
			return lstPos16;
		else if ((row == 2) && (col == 8))
			return lstPos17;
		else if ((row == 2) && (col == 9))
			return lstPos18;

		else if ((row == 3) && (col == 1))
			return lstPos19;
		else if ((row == 3) && (col == 2))
			return lstPos20;
		else if ((row == 3) && (col == 3))
			return lstPos21;
		else if ((row == 3) && (col == 4))
			return lstPos22;
		else if ((row == 3) && (col == 5))
			return lstPos23;
		else if ((row == 3) && (col == 6))
			return lstPos24;
		else if ((row == 3) && (col == 7))
			return lstPos25;
		else if ((row == 3) && (col == 8))
			return lstPos26;
		else if ((row == 3) && (col == 9))
			return lstPos27;

		else if ((row == 4) && (col == 1))
			return lstPos28;
		else if ((row == 4) && (col == 2))
			return lstPos29;
		else if ((row == 4) && (col == 3))
			return lstPos30;
		else if ((row == 4) && (col == 4))
			return lstPos31;
		else if ((row == 4) && (col == 5))
			return lstPos32;
		else if ((row == 4) && (col == 6))
			return lstPos33;
		else if ((row == 4) && (col == 7))
			return lstPos34;
		else if ((row == 4) && (col == 8))
			return lstPos35;
		else if ((row == 4) && (col == 9))
			return lstPos36;

		else if ((row == 5) && (col == 1))
			return lstPos37;
		else if ((row == 5) && (col == 2))
			return lstPos38;
		else if ((row == 5) && (col == 3))
			return lstPos39;
		else if ((row == 5) && (col == 4))
			return lstPos40;
		else if ((row == 5) && (col == 5))
			return lstPos41;
		else if ((row == 5) && (col == 6))
			return lstPos42;
		else if ((row == 5) && (col == 7))
			return lstPos43;
		else if ((row == 5) && (col == 8))
			return lstPos44;
		else if ((row == 5) && (col == 9))
			return lstPos45;

		else if ((row == 6) && (col == 1))
			return lstPos46;
		else if ((row == 6) && (col == 2))
			return lstPos47;
		else if ((row == 6) && (col == 3))
			return lstPos48;
		else if ((row == 6) && (col == 4))
			return lstPos49;
		else if ((row == 6) && (col == 5))
			return lstPos50;
		else if ((row == 6) && (col == 6))
			return lstPos51;
		else if ((row == 6) && (col == 7))
			return lstPos52;
		else if ((row == 6) && (col == 8))
			return lstPos53;
		else if ((row == 6) && (col == 9))
			return lstPos54;

		else if ((row == 7) && (col == 1))
			return lstPos55;
		else if ((row == 7) && (col == 2))
			return lstPos56;
		else if ((row == 7) && (col == 3))
			return lstPos57;
		else if ((row == 7) && (col == 4))
			return lstPos58;
		else if ((row == 7) && (col == 5))
			return lstPos59;
		else if ((row == 7) && (col == 6))
			return lstPos60;
		else if ((row == 7) && (col == 7))
			return lstPos61;
		else if ((row == 7) && (col == 8))
			return lstPos62;
		else if ((row == 7) && (col == 9))
			return lstPos63;

		else if ((row == 8) && (col == 1))
			return lstPos64;
		else if ((row == 8) && (col == 2))
			return lstPos65;
		else if ((row == 8) && (col == 3))
			return lstPos66;
		else if ((row == 8) && (col == 4))
			return lstPos67;
		else if ((row == 8) && (col == 5))
			return lstPos68;
		else if ((row == 8) && (col == 6))
			return lstPos69;
		else if ((row == 8) && (col == 7))
			return lstPos70;
		else if ((row == 8) && (col == 8))
			return lstPos71;
		else if ((row == 8) && (col == 9))
			return lstPos72;

		else if ((row == 9) && (col == 1))
			return lstPos73;
		else if ((row == 9) && (col == 2))
			return lstPos74;
		else if ((row == 9) && (col == 3))
			return lstPos75;
		else if ((row == 9) && (col == 4))
			return lstPos76;
		else if ((row == 9) && (col == 5))
			return lstPos77;
		else if ((row == 9) && (col == 6))
			return lstPos78;
		else if ((row == 9) && (col == 7))
			return lstPos79;
		else if ((row == 9) && (col == 8))
			return lstPos80;
		else
			return lstPos81;
	}

	private void insertElement(List<Integer> lst, int noToBeInserted) {
		if (!lst.contains(noToBeInserted))
			lst.add(noToBeInserted);
	}

	private void removeElementFromList(List<?> lst, int noToBeDeleted) {

		if (lst.contains(noToBeDeleted))
			lst.remove(lst.indexOf(noToBeDeleted));

		System.out.println("Deleted: " + noToBeDeleted + " from List " + lst);

		if (lst.size() == 1) {
			System.out.println("Lbl mapping: " + mapListsToLables(lst));
			mapListsToLables(lst).setText("" + lst.get(0));

			solveSuDoKu();
		}// yoy
	}

	private JLabel mapListsToLables(List<?> lst) {
		if (lst == lstPos1)
			return lblArray[1][1];
		if (lst == lstPos2)
			return lblArray[1][2];
		if (lst == lstPos3)
			return lblArray[1][3];
		if (lst == lstPos4)
			return lblArray[1][4];
		if (lst == lstPos5)
			return lblArray[1][5];
		if (lst == lstPos6)
			return lblArray[1][6];
		if (lst == lstPos7)
			return lblArray[1][7];
		if (lst == lstPos8)
			return lblArray[1][8];
		if (lst == lstPos9)
			return lblArray[1][9];
		if (lst == lstPos10)
			return lblArray[2][1];

		if (lst == lstPos11)
			return lblArray[2][2];
		if (lst == lstPos12)
			return lblArray[2][3];
		if (lst == lstPos13)
			return lblArray[2][4];
		if (lst == lstPos14)
			return lblArray[2][5];
		if (lst == lstPos15)
			return lblArray[2][6];
		if (lst == lstPos16)
			return lblArray[2][7];
		if (lst == lstPos17)
			return lblArray[2][8];
		if (lst == lstPos18)
			return lblArray[2][9];
		if (lst == lstPos19)
			return lblArray[3][1];
		if (lst == lstPos20)
			return lblArray[3][2];

		if (lst == lstPos21)
			return lblArray[3][3];
		if (lst == lstPos22)
			return lblArray[3][4];
		if (lst == lstPos23)
			return lblArray[3][5];
		if (lst == lstPos24)
			return lblArray[3][6];
		if (lst == lstPos25)
			return lblArray[3][7];
		if (lst == lstPos26)
			return lblArray[3][8];
		if (lst == lstPos27)
			return lblArray[3][9];
		if (lst == lstPos28)
			return lblArray[4][1];
		if (lst == lstPos29)
			return lblArray[4][2];
		if (lst == lstPos30)
			return lblArray[4][3];

		if (lst == lstPos31)
			return lblArray[4][4];
		if (lst == lstPos32)
			return lblArray[4][5];
		if (lst == lstPos33)
			return lblArray[4][6];
		if (lst == lstPos34)
			return lblArray[4][7];
		if (lst == lstPos35)
			return lblArray[4][8];
		if (lst == lstPos36)
			return lblArray[4][9];
		if (lst == lstPos37)
			return lblArray[5][1];
		if (lst == lstPos38)
			return lblArray[5][2];
		if (lst == lstPos39)
			return lblArray[5][3];
		if (lst == lstPos40)
			return lblArray[5][4];

		if (lst == lstPos41)
			return lblArray[5][5];
		if (lst == lstPos42)
			return lblArray[5][6];
		if (lst == lstPos43)
			return lblArray[5][7];
		if (lst == lstPos44)
			return lblArray[5][8];
		if (lst == lstPos45)
			return lblArray[5][9];
		if (lst == lstPos46)
			return lblArray[6][1];
		if (lst == lstPos47)
			return lblArray[6][2];
		if (lst == lstPos48)
			return lblArray[6][3];
		if (lst == lstPos49)
			return lblArray[6][4];
		if (lst == lstPos50)
			return lblArray[6][5];

		if (lst == lstPos51)
			return lblArray[6][6];
		if (lst == lstPos52)
			return lblArray[6][7];
		if (lst == lstPos53)
			return lblArray[6][8];
		if (lst == lstPos54)
			return lblArray[6][9];
		if (lst == lstPos55)
			return lblArray[7][1];
		if (lst == lstPos56)
			return lblArray[7][2];
		if (lst == lstPos57)
			return lblArray[7][3];
		if (lst == lstPos58)
			return lblArray[7][4];
		if (lst == lstPos59)
			return lblArray[7][5];
		if (lst == lstPos60)
			return lblArray[7][6];

		if (lst == lstPos61)
			return lblArray[7][7];
		if (lst == lstPos62)
			return lblArray[7][8];
		if (lst == lstPos63)
			return lblArray[7][9];
		if (lst == lstPos64)
			return lblArray[8][1];
		if (lst == lstPos65)
			return lblArray[8][2];
		if (lst == lstPos66)
			return lblArray[8][3];
		if (lst == lstPos67)
			return lblArray[8][4];
		if (lst == lstPos68)
			return lblArray[8][5];
		if (lst == lstPos69)
			return lblArray[8][6];
		if (lst == lstPos70)
			return lblArray[8][7];

		if (lst == lstPos71)
			return lblArray[8][8];
		if (lst == lstPos72)
			return lblArray[8][9];
		if (lst == lstPos73)
			return lblArray[9][1];
		if (lst == lstPos74)
			return lblArray[9][2];
		if (lst == lstPos75)
			return lblArray[9][3];
		if (lst == lstPos76)
			return lblArray[9][4];
		if (lst == lstPos77)
			return lblArray[9][5];
		if (lst == lstPos78)
			return lblArray[9][6];
		if (lst == lstPos79)
			return lblArray[9][7];
		if (lst == lstPos80)
			return lblArray[9][8];

		else
			return lblArray[9][9];
	}

	public static void main(String args[]) {
		new SuDoKu();
	}
}
